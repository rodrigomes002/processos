﻿using Processos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Processos.Domain.Interfaces.Repositories
{
    public interface IProcessosRepository
    {
        Task Insert(Processo processo);
        Task<IEnumerable<Processo>> SelectAll();
        Task<Processo> Select(Guid id);
        bool ExisteNumeroDoProcesso(string numeroDoProcesso);
    }
}
