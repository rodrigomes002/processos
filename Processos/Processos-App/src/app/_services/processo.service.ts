import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Processo } from '../_models/Processo';

@Injectable({
  providedIn: 'root'
})
export class ProcessoService {
  baseUrl = 'https://localhost:44305/api/processos';

  constructor(private http: HttpClient) { }

  getAllProcessos(): Observable<Processo[]> {
    return this.http.get<Processo[]>(this.baseUrl);
  }
}
