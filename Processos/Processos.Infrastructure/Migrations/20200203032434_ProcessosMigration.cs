﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Processos.Infrastructure.Migrations
{
    public partial class ProcessosMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Processos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    NumeroDoProcesso = table.Column<string>(nullable: true),
                    DataDeCriacaoDoProcesso = table.Column<DateTime>(nullable: false),
                    Valor = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Escritorio = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Processos", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Processos");
        }
    }
}
