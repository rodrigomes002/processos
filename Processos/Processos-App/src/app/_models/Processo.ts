export interface Processo {
    numeroDoProcesso: string;
    dataDeCriacaoDoProcesso: Date;
    valor: number;
    escritorio: string;
}
