﻿using FluentValidation;
using Processos.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Processos.Domain.Interfaces.Services
{
    public interface IProcessosService
    {
        Task Post<V>(Processo processo) where V : AbstractValidator<Processo>;
        Task<Processo> GetProcessoById(Guid id);
        Task<IEnumerable<Processo>> GetAllProcessos();
    }
}
