# Desafio para vaga de desenvolvedor da Perlink

Solução Encontrada.
Foi criado uma API Rest para realizar a criação, busca e persistência dos Processos.
Foi criado uma página Web para exibir as listas de Processos.

### Tecnologias utilizadas.
```
.NET Core 2.2
Entity Framework Core
Angular2+
Bootstrap
FluentValidations
```

### Instruções de uso
Os escritórios que forem gerar os processos devem realizar uma requisição POST para a url <url_api_aqui>api/processos, realizando um envio de um JSON com a seguinte estrutura:
{
   "numeroDoProcesso": "312312312",
   "dataDeCriacaoDoProcesso": 03-02-2020,
   "valor": 1500.12
   "escritorio" : "Nome do escritório"
}

Caso ocorra de um escritório enviar o mesmo número do processo, é emitido um aviso de que o processo não será persistido.
Caso algum dos campos não sejam informados é emitido um aviso de que o processo não será persistido.

Para acessar o site, deve ser feito o acesso pela URL <url_site_aqui>/, a página contém uma lista de todos os processos que foram registrados e é atualizada a cada 10 segundos com os novos processos recebidos.

### Ideias que gostaria de implementar caso tivesse mais tempo.

Melhorias no geral na interface do front-end disponível para o usuário, filtros, pesquisa paginação, por ter um 100 processos por segundo sendo disparados de diversos escritórios, o ideal seria que tivesse algo que tornasse a tela mais "clean" para o usuário e que ele pudesse acompanhar certos processos a escolher.