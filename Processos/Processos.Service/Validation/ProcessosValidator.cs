﻿using FluentValidation;
using Processos.Domain.Entities;
using Processos.Domain.Interfaces.Repositories;
using Processos.Infrastructure;
using Processos.Infrastructure.Persistence.Repositories;
using System;

namespace Processos.Service.Validation
{
    public class ProcessosValidator : AbstractValidator<Processo>
    {
       
        public ProcessosValidator()
        {
            RuleFor(c => c).NotNull().OnAnyFailure(x =>
            {
                throw new ArgumentNullException("Objeto não informado.");
            });

            RuleFor(c => c.NumeroDoProcesso)
                .NotEmpty().WithMessage("O número do processo é obrigatório.")
                .NotNull().WithMessage("O número do processo é obrigatório.");

            RuleFor(c => c.DataDeCriacaoDoProcesso)
                .NotEmpty().WithMessage("A data de criação do processo é obrigatória.")
                .NotNull().WithMessage("A data de criação do processo é obrigatória.");

            RuleFor(c => c.Valor)
                .NotEmpty().WithMessage("O valor do processo é obrigatório.")
                .NotNull().WithMessage("O valor do processo é obrigatório.");

            RuleFor(c => c.Escritorio)
                .NotEmpty().WithMessage("O escritório é obrigatório.")
                .NotNull().WithMessage("O escritório é obrigatório.");
        }
    }
}
