﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Processos.Domain.Entities;
using Processos.Domain.Interfaces.Services;
using Processos.Service.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Processos.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessosController : ControllerBase
    {
        private readonly IProcessosService _service;

        public ProcessosController(IProcessosService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Processo>> Get(Guid id)
        {
            var processo = await _service.GetProcessoById(id);

            if (processo == null)
            {
                return NotFound();
            }

            return processo;
        }

        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Processo>>> Get()
        {
            var processos = await _service.GetAllProcessos();
            return Ok(processos);
        }

        
        [HttpPost]
        public async Task<ActionResult> Post(Processo processo)
        {
            try
            {
                await _service.Post<ProcessosValidator>(processo);
                return CreatedAtAction("Get", processo);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Não foi possivel inserir!");
            }
        }
    }
}
