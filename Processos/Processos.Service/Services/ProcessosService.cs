﻿using FluentValidation;
using Processos.Domain.Entities;
using Processos.Domain.Interfaces.Repositories;
using Processos.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Processos.Service.Services
{
    public class ProcessosService : IProcessosService
    {
        private readonly IProcessosRepository _repository;

        public ProcessosService(IProcessosRepository repository)
        {
            _repository = repository;
        }

        private void Validate(Processo processo, AbstractValidator<Processo> validator)
        {
            if (processo == null)
                throw new Exception("Objeto não encontrado!");

            validator.ValidateAndThrow(processo);
        }


        public async Task Post<V>(Processo processo) where V : AbstractValidator<Processo>
        {
            Validate(processo, Activator.CreateInstance<V>());

            if (_repository.ExisteNumeroDoProcesso(processo.NumeroDoProcesso))
            {
                throw new Exception("Já existe este número de processo.");
            }
            else
            {
                await _repository.Insert(processo);
            }
        }

        public async Task<IEnumerable<Processo>> GetAllProcessos()
        {
            return await _repository.SelectAll();
        }

        public async Task<Processo> GetProcessoById(Guid id)
        {
            return await _repository.Select(id);
        }
    }
}
