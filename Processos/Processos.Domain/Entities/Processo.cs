﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Processos.Domain.Entities
{
    public class Processo
    {

        public Guid Id { get; set; }
        public string NumeroDoProcesso { get; set; }
        public DateTime DataDeCriacaoDoProcesso { get; set; }
        
        [Column(TypeName="decimal(18,2)")]
        public decimal Valor { get; set; }        
        public string Escritorio { get; set; }

        public Processo(string numeroDoProcesso, DateTime dataDeCriacaoDoProcesso, decimal valor, string escritorio)
        {
            Id = Guid.NewGuid();
            NumeroDoProcesso = numeroDoProcesso;
            DataDeCriacaoDoProcesso = dataDeCriacaoDoProcesso;
            Valor = valor;
            Escritorio = escritorio;
        }
    }
}
