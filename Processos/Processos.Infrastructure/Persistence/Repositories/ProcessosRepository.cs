﻿using Microsoft.EntityFrameworkCore;
using Processos.Domain.Entities;
using Processos.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Processos.Infrastructure.Persistence.Repositories
{
    public class ProcessosRepository : IProcessosRepository
    {
        private readonly ProcessosContext _context;

        public ProcessosRepository(ProcessosContext context)
        {
            _context = context;
        }

        public async Task Insert(Processo processo)
        {
            _context.Add(processo);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Processo>> SelectAll()
        {
            return await _context.Processos.ToArrayAsync();
        }
        public async Task<Processo> Select(Guid id)
        {
            return await _context.Processos.FindAsync(id);
        }

        public bool ExisteNumeroDoProcesso(string numeroDoProcesso)
        {
            return _context.Processos.Any(x => x.NumeroDoProcesso == numeroDoProcesso);
        }        
    }
}
