﻿using Microsoft.EntityFrameworkCore;
using Processos.Domain.Entities;

namespace Processos.Infrastructure
{
    public class ProcessosContext : DbContext
    {
        public ProcessosContext(DbContextOptions<ProcessosContext> options)
            : base(options)
        {
        }
        public DbSet<Processo> Processos { get; set; }
        
    }
}
