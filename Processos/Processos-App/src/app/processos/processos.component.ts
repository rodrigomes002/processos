import { Component, OnInit } from '@angular/core';
import { ProcessoService } from '../_services/processo.service';
import { Processo } from '../_models/Processo';

@Component({
  selector: 'app-processos',
  templateUrl: './processos.component.html',
  styleUrls: ['./processos.component.css']
})
export class ProcessosComponent implements OnInit {

  processos: Processo[];

  constructor(private processoService: ProcessoService) { }

  ngOnInit() {
    this.getProcessos();
  }

  getProcessos() {
    this.processoService.getAllProcessos().subscribe(
      (_processos: Processo[]) => {
        this.processos = _processos;
      }, error => {
        console.log(error);
      });
  }
}
